<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PoliceStation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('police_station',function (Blueprint $table){
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('province');
            $table->string('district');
            $table->string('sub_district');
            $table->integer('hospcode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('police_station');
    }
}
