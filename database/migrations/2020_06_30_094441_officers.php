<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Officers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('officers',function (Blueprint $table){
            $table->bigIncrements('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->integer('hospcode');
            $table->string('email');
            $table->string('phone_number');
            $table->string('cid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('officers');
    }
}
