<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class doctors extends Sximo  {
	
	protected $table = 'doctors';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT doctors.* FROM doctors  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE doctors.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
