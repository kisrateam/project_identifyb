

		 {!! Form::open(array('url'=>'hospital', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Hospital</legend>
									
									  <div class="form-group  " >
										<label for="Hospcode" class=" control-label col-md-4 text-left"> Hospcode <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='hospcode' id='hospcode' value='{{ $row['hospcode'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Name" class=" control-label col-md-4 text-left"> Name <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='name' id='name' value='{{ $row['name'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Typecode" class=" control-label col-md-4 text-left"> Typecode <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='typecode' id='typecode' value='{{ $row['typecode'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Type" class=" control-label col-md-4 text-left"> Type <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='type' id='type' value='{{ $row['type'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Ministry" class=" control-label col-md-4 text-left"> Ministry <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='ministry' id='ministry' value='{{ $row['ministry'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Ministryname" class=" control-label col-md-4 text-left"> Ministryname <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='ministryname' id='ministryname' value='{{ $row['ministryname'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Departcode" class=" control-label col-md-4 text-left"> Departcode <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='departcode' id='departcode' value='{{ $row['departcode'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Depart" class=" control-label col-md-4 text-left"> Depart <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='depart' id='depart' value='{{ $row['depart'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Changwatcode" class=" control-label col-md-4 text-left"> Changwatcode <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='changwatcode' id='changwatcode' value='{{ $row['changwatcode'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Ampurcode" class=" control-label col-md-4 text-left"> Ampurcode <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='ampurcode' id='ampurcode' value='{{ $row['ampurcode'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Tamboncode" class=" control-label col-md-4 text-left"> Tamboncode <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='tamboncode' id='tamboncode' value='{{ $row['tamboncode'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Tel" class=" control-label col-md-4 text-left"> Tel <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='tel' id='tel' value='{{ $row['tel'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Zip" class=" control-label col-md-4 text-left"> Zip <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='zip' id='zip' value='{{ $row['zip'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Type1" class=" control-label col-md-4 text-left"> Type1 <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='type1' id='type1' value='{{ $row['type1'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Region" class=" control-label col-md-4 text-left"> Region <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='region' id='region' value='{{ $row['region'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Active" class=" control-label col-md-4 text-left"> Active <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='Active' id='Active' value='{{ $row['Active'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-default btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-default btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 <input type="hidden" name="action_task" value="public" />
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
