<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class policestation extends Sximo  {
	
	protected $table = 'police_station';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT police_station.* FROM police_station  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE police_station.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
