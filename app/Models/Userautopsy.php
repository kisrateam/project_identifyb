<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class userautopsy extends Sximo  {
	
	protected $table = 'users_autopsy';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT users_autopsy.* FROM users_autopsy  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE users_autopsy.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
