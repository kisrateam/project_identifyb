<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class injurytype extends Sximo  {
	
	protected $table = 'injury_type';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT injury_type.* FROM injury_type  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE injury_type.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
